*** Settings ***
Library	String
Library	Collections
Library	OperatingSystem
Library	SeleniumLibrary

Resource    webui_utils.robot
Resource    navigation_utils.robot

Resource    ../configs/global_var.robot

*** Variables ***


*** Keywords ***     

Read Test Data From Json
    [Arguments]    ${json_file}
    Log To Console    ${json_file}    
    ${TEST_DATA}=    Get Dictionary From Json    ${json_file}
    
    Log Dictionary    ${TEST_DATA}
    Set Suite Variable    ${TEST_DATA}
        
Reload Page on Failure
    Log To Console    ${TEST STATUS}
    Run keyword IF    '${TEST STATUS}' == 'FAIL'     Reload Page                
       
Upload File
    [Arguments]    ${xpath}    ${file}
    Log To Console    ${CURDIR} 
    Set Focus To Element    ${xpath}       
    Choose File     ${xpath} 	${CURDIR}/${file}
    Sleep    3s