***Setting***
Library    SeleniumLibrary
Library    OperatingSystem

Resource    ../../utils/test_data_parse_utils.robot
Resource    ../../utils/webui_utils.robot

*** Variables ***

${user_xpath}    xpath=//*[@id="login-form-username"]
${pass_xpath}    xpath=//*[@id="login-form-password"]
${login_btn_xpath}    xpath=//*[@id="login"]
${login_testdata_path}    /configs/config.json

*** Keywords ***

Login To Application
    ${login_test_data}=    Get Dictionary From Json    ${login_testdata_path}
    Open Browser	${login_test_data['url']}    ${login_test_data['browser']}

    Input Text    ${user_xpath}    ${login_test_data['email']}
    Input Text    ${pass_xpath}    ${login_test_data['password']}
    Click Element    ${login_btn_xpath}
    # Wait Until Page Contains    Dashboard    30    
    # Set Window Size    1920    1080
    Sleep    20s
    # Maximize Browser Window
    