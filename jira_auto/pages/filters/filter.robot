*** Settings ***
Library	String
Library	Collections
Library	OperatingSystem
Library	SeleniumLibrary

Resource    ../../webui_utils.robot
Resource    ../../navigation_utils.robot

Resource    ../../configs/global_var.robot

*** Variables ***


*** Keywords ***
Find Story List From Filter Page
    [Arguments]    ${filter}
    # Click Link    xpath=//a[@id="filter_lnk_23167_lnk"]
    # Sleep    10s
    ${no_of_issues} =  Get Element Count    xpath=//div/ol/li
    @{stories}=    Create List
    # :FOR    ${issue}    IN    @{issues}
    :FOR    ${i}    IN RANGE    1    ${no_of_issues} - 3
    \    ${issue}=    Get Text    xpath=//div/ol/li[${i}]/a/span
    \    ${issue}=    Strip String    ${issue} 
    \    log to console    *${issue}*
    \    ${img}=    Get Element Attribute    xpath=//div/ol/li[${i}]/a/span[1]/img    src
    \    log to console     ${img}
    # \    ${bool}=    String Contains    ${img}    14675
    # \    ${contains}=  Evaluate   "14675"    IN    """${img}"""
    \    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${img}    14675
    \    ${contains}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    \    Run Keyword If    ${contains}    Append To List   ${stories}    ${issue}
    [Return]    @{stories}        
    
Get Linked Tests From Story
    [Arguments]    ${story}
    # //div/dl[@class="links-list "]/dd
    @{tests}=    Create List
    Click Element    xpath=//div/ol/li/a/*[contains(text(),'${story}')]
    Sleep    10s
    ${linked_tests} =  Get Element Count    xpath=//div/dl[@class="links-list "]/dd
    # :FOR    ${issue}    IN    @{issues}
    :FOR    ${i}    IN RANGE    1    ${linked_tests}
    \    ${img}=    Get Element Attribute    xpath=//div/dl[@class="links-list "]/dd/div/p/img
    \    Log To Console     ${img}
    \    
    \    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${img}    ico_zephyr_issuetype.png
    \    ${contains}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    \    ${test}=    Set Variable If    ${contains}    Get Text    xpath=//div/dl[@class="links-list "]/dd/div/p/span/a
    \    Log To Console    ${test} 
    \    Run Keyword If    ${contains}    Append To List   ${tests}    ${test}
    [Return]    @{tests} 

        
Get Linked Tests
    [Arguments]    ${i}
    # //div/dl[@class="links-list "]/dd
    Click Element    xpath=//div/ol/li[${i}]/a/span[1]
    Sleep    10s
    ${linked_tests} =  Get Element Count    xpath=//div/dl[@class="links-list "]/dd
    # :FOR    ${issue}    IN    @{issues}
    :FOR    ${i}    IN RANGE    1    ${linked_tests}
    \    ${img}=    Get Element Attribute    xpath=//div/dl[@class="links-list "]/dd/div/p/img
    \    Log To Console     ${img}
    \    
    \    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${img}    ico_zephyr_issuetype.png
    \    ${contains}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    \    ${test}=    Set Variable If    ${contains}    Get Text    xpath=//div/dl[@class="links-list "]/dd/div/p/span/a
    \    Log To Console    ${test} 