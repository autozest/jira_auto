*** Settings ***
Library    String
Library    Collections
Library    OperatingSystem
Library    SeleniumLibrary
Library    OperatingSystem
Library    RequestsLibrary 

*** Keywords ***

Get Home Dir
    ${home_dir}=    Set Variable    ${CURDIR}/../
    [return]    ${home_dir}
    
Get Dictionary From Json
    [Arguments]    ${file_path}
    ${home_dir}=	Get Home Dir
    ${master_testbed_file}=    Get File    ${home_dir}${file_path}
    ${testbed_file}=	To Json    ${master_testbed_file}
    [Return]    ${testbed_file}