***Setting***
Library    SeleniumLibrary
Library    OperatingSystem
Resource    ../../../utils/test_data_parse_utils.robot
Resource    ../../../utils/webui_utils.robot
Resource    ../../../pages/login/login.robot
Resource    ../../../utils/navigation_utils.robot

*** Variables ***
${testdata_path}    /configs/master_testdata/testdata.json

*** Keywords ***
Temporary Keyword
    ${data_dict}=    Get Dictionary From Json    ${testdata_path}
    ${job_test_data}=    Get From Dictionary    ${data_dict}    XYZ
    Log Dictionary    ${data_dict}
    