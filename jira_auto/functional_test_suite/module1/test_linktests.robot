***Setting***
Library    SeleniumLibrary
Library    OperatingSystem
Library    String

Resource    ../../../utils/test_data_parse_utils.robot
Resource    ../../../utils/webui_utils.robot
Resource    ../../../pages/login/login.robot
Resource    ../../../utils/navigation_utils.robot
Resource    ../../pages/login/login.robot
Resource    ../../pages/filters/filter.robot
Resource    ../../common.robot



Suite Setup    Run Keywords  
# ...    Read Contact Test Data From Json
...    Login To Application

Test Teardown    Run Keywords
# ...    Clean Up    
...    Reload Page on Failure

Suite Teardown    Close All Browsers

*** Test Cases ***

# Test1
    # [Tags]    Smoke    Regression
    # Click Element    xpath=//a[@id="find_link"]
    # Sleep    10s  
    # Click Link    xpath=//a[@id="filter_lnk_23167_lnk"]
    # Sleep    10s 
    # ${issues} =  Get Element Count    xpath=//div/ol/li
    # # :FOR    ${issue}    IN    @{issues}
    # :FOR    ${i}    IN RANGE    1    ${issues} - 3
    # \    ${issue}=    Get Text    xpath=//div/ol/li[${i}]/a 
    # \    log to console    ${issue}
    # \    ${img}=    Get Element Attribute    xpath=//div/ol/li[${i}]/a/span[1]/img    src
    # \    log to console     ${img}
    # # \    ${bool}=    String Contains    ${img}    14675
    # # \    ${contains}=  Evaluate   "14675"    IN    """${img}"""
    # \    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${img}    14675
    # \    ${contains}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    # \    Run Keyword If    ${contains}    Get Linked Tests    ${i}
    # \    
              
    # # \    //div/dl[@class="links-list "]/dd
    
Get The List Of Tests From The User Story      
    [Tags]    Smoke
   Click Element    xpath=//a[@id="find_link"]
    Sleep    10s  
    Click Link    xpath=//a[@id="filter_lnk_23167_lnk"]
    Sleep    10s  
    
    ${issues} =  Find Story List From Filter Page    Post M3    
    :FOR    ${issue}    IN    @{issues}
    \    Log To Console    ${issue}
    \    Get Linked Tests From Story    ${issue}
# Test3
    # [Tags]    Smoke    Regression     

# Test4
    # [Tags]    Regression  