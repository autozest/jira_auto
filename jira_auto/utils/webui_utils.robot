*** Settings ***
Library	String
Library	Collections
Library	OperatingSystem
Library	SeleniumLibrary

*** Variables ***

${small_wait}    10s
${medium_wait}    15s
${long_wait}    20s

*** Keywords ***
Select Date
    pass

Add Value Into Field
    [Arguments]    ${elem_path}    ${data_dict}    ${field_name}
    ${status}=    Run Keyword And Return Status    Dictionary Should Contain Key    ${data_dict}    ${field_name}
	
	Return From Keyword IF    '${status}' != 'True'
    Input In Visible Element    ${elem_path}    ${data_dict['${field_name}']} 

Click Visible Element
    [Arguments]    ${elem_path}    ${wait_time}=${small_wait}
    Wait Until Element Is Enabled    ${elem_path}    ${wait_time}
    
    Set Focus To Element    ${elem_path}
    Wait Until Element Is Visible    ${elem_path}    ${wait_time}   
    Click Element    ${elem_path}
      
Select From Dropdown Suggestion
    [Arguments]    ${name}
    
    
Select Value From Checkbox New
    [Arguments]    ${elem_path}    ${inp_txt}    
    
    Click Visible Element    ${elem_path} 

Scroll Element Into View
    [Arguments]    ${element}
    ${argument}=    Set Variable    window.document.evaluate("${element}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
    Execute Javascript    ${argument}
    
 Select From AutoSuggestion
     [Arguments]    ${xpath}    ${value}
     Input In Visible Element    ${xpath}    ${value}
     Sleep    2s
     Select From Dropdown Suggestion    ${value}
     Sleep    3s

